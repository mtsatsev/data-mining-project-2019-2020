import numpy as np
from utility import *
from os import *
import pandas as pd
import tensorflow as tf
import datetime
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.callbacks import TensorBoard
import cv2
import matplotlib.pyplot as plt
n = 4000

def load_data(folder):
    train_data = []
    for filename in listdir(folder):
        img = color_image_gray(folder,filename)
        if len(train_data) >= n:
            return train_data
        if img is not None:
            thresh = convert_image_into_binary_image(img)
            contour = find_countours(img)
            countors = sorted(contour[0], key=lambda ctr: cv2.boundingRect(ctr)[-1])
            h = 28
            w = 28
            maximum = 0
            for c in countors:
                x, y, z, a = cv2.boundingRect(c)
                maximum = max(h*w,maximum)
                if maximum == w*h:
                    max_x = x
                    max_y = y
                    max_z = z
                    max_a = a
            cut_img = thresh[max_y:y+max_a+10,max_x:max_x+max_z+10]
            resize_img = cv2.resize(cut_img,(28,28))
            resize_img = np.reshape(resize_img,(784,1))
            train_data.append(resize_img)

    return train_data

data = load_data('digit_images/+')
for i in range(0,n):
    data[i] = np.append(data[i],['10'])

data = load_data('digit_images/minus')
for i in range(0, n):
    data[i] = np.append(data[i], ['11'])

def create_csv():
    data_ = np.concatenate((data, data))
    for i in range(10):
        mnist = load_data('digit_images/'+str(i)+'')
        for j in range(0,len(mnist)):
            mnist[j] = np.append(mnist[j],[str(i)])
        data_ = np.concatenate((data_,mnist))
    return data_

data = create_csv()
data = pd.DataFrame(data,index=None)
data.to_csv('mnist.csv',index=False)

DATA = pd.read_csv('mnist.csv',index_col=False)
DATA = DATA.iloc[n:]
labels = DATA[['784']]
DATA = DATA.drop('784',axis=1)
X = DATA.values

y_train = tf.keras.utils.to_categorical(labels,num_classes=12)
x_train = []
for i in range(len(X)):
    x_train.append(np.array(X[i:i+1]).reshape((28,28)))

x_train = np.array(x_train)
y_train = np.array(y_train)

Name = "Handwritten equation solver ".format(datetime.time())
PATH = getcwd()
logdir = PATH + "digit/log-1"
tensortboard = TensorBoard(log_dir=logdir.format(Name))

np.random.seed(7)
network = Sequential()
network.add(Flatten(input_shape=(28, 28)))
network.add(Dense(128,activation=tf.nn.relu,input_shape=(28, 28)))
network.add(Dense(12,activation=tf.nn.softmax))

network.compile(optimizer=tf.optimizers.Adam(),
                loss=tf.keras.losses.CategoricalCrossentropy(),
                metrics=[tf.keras.metrics.Accuracy()],
                verbose=1,
                shuffle=True)

network.fit(x_train,y_train,epochs=15,batch_size=20)
print("__________________________________________________________________________________________:(")
score = network.evaluate(x_train,y_train,verbose=0)
print("THATS ALL FOLKS")