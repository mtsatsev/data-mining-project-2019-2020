import cv2
from os import *

def color_image_gray(folder,filename):
    image = cv2.imread(path.join(folder,filename))
    image2 = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    return ~image2

def convert_image_into_binary_image(img):
    ret,bw_img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
    return bw_img

def find_countours(image):
    return cv2.findContours(image,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)