import pandas as pd
import numpy as np
import tensorflow as tf

n = 4000
DATA = pd.read_csv('mnist.csv',index_col=False)
labels = DATA[['784']]
DATA = DATA.drop('784',axis=1)

with open('output.tsv','w') as out:
   out.write(DATA.to_csv(sep='\t', index=False))
