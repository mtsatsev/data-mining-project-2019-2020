import numpy as np
from utility import *
import cv2
from tensorflow.keras.models import*
import matplotlib.pyplot as plt

model = load_model("model.h5")
print("give path to image like: example.jpg")
x = input()
c = color_image_gray("",x)
c = convert_image_into_binary_image(c)
cnts = find_countours(c)
cnts = cnts[0] if len(cnts) == 2 else cnts[1]
cnts = sorted(cnts, key=cv2.contourArea,reverse=True)

testc = []
for i in cnts:
    x, y, w, h = cv2.boundingRect(i)
    testc.append(c[y:y+h+10, x:x+w+10])

proper_image = []
for i in range (3):
    resize_img = cv2.resize(testc[i],(28,28))
    resize_img = np.reshape(resize_img,(784,1))
    proper_image.append(np.array(resize_img).reshape((28,28)))

proper_image = np.array(proper_image)
print(proper_image[0].shape)

predictions = model.predict(proper_image)

x = np.argmax(predictions[0])
y = np.argmax(predictions[1])

sign = np.argmax(predictions[2])
plt.hist([x,y,sign],[0,1,2,3,4,5,6,7,8,9,10,11])
plt.show()
if (sign == 1):
    result = x + y
elif(sign == 0):
    result = x - y

print(result)